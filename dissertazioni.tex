\documentclass[11pt, a4paper]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}

\usepackage{fullpage}

% Diciamo al TeX che ci fa schifo avere righe singole di testo all'inizio o a fine pagina
\widowpenalty=10000
\clubpenalty=10000

\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{commath}
\usepackage{booktabs}
\usepackage[colorlinks]{hyperref}
\usepackage{tikz}
\usepackage{pgfplots}

\newcommand{\R}{\mathbb{R}}

\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\varvec}[1]{\boldsymbol{#1}}
\renewcommand{\epsilon}{\varepsilon}
\renewcommand{\theta}{\vartheta}
\renewcommand{\div}[1]{\mathrm{div}{#1}}
\newcommand{\rot}[1]{\mathrm{rot}{#1}}

\theoremstyle{plain}
\newtheorem{dissertazione}{D}
\makeatletter
\renewenvironment{proof}[1][Risposta]{\par
    \pushQED{}%
    \normalfont \topsep6\p@\@plus6\p@\relax
    \trivlist
    \item\relax
    {\itshape
        #1\@addpunct{.}}\hspace\labelsep\ignorespaces
}{%
    \popQED\endtrivlist\@endpefalse
    \addvspace{15pt plus 15pt}
}
\makeatother

\title{Dissertazioni per l'esame di Elettrodinamica Classica}
\author{Alessandro Piazza \thanks{alessandro.piazza@sns.it}}

\begin{document}
\maketitle

\begin{abstract}
    Svolgimento delle dissertazioni più frequenti del compito sulla seconda parte del corso di Elettrodinamica Classica. Per le risposte ho preso spunto da simili elaborati scritti da Fabio Zoratti e Giuseppe Bogna. \\

    Quest'opera è stata rilasciata con licenza Creative Commons Attribuzione-Condividi allo stesso modo 4.0 Internazionale. Per leggere una copia della licenza visita il sito web \url{http://creativecommons.org/licenses/by-sa/4.0/}.
    \href{http://creativecommons.org/licenses/by-sa/4.0/}{\includegraphics[scale=0.8]{by-sa}}
\end{abstract}

\begin{dissertazione} \label{diss:energia}
	Velocità di gruppo e propagazione dell'energia elettromagnetica nei mezzi trasparenti e dispersivi.
\end{dissertazione}
\begin{proof}
	Se ci limitiamo ad un'onda piana in un mezzo trasparente e non dispersivo, l'unica velocità che conosce è $ \omega / k = c/n $. Tuttavia nel caso di materiali dispersivi $ n = n(\omega) $ e pertanto tale velocità dipende dalla frequenza. Inoltre per descrivere il trasporto di energia è utile considerare un pacchetto d'onda invece di un'onda piana, in quanto essendo quest'ultima infinita sarà difficile capire a che velocità si sta muovendo l'energia.

	Lavoriamo per semplicità in una dimensione. Supponiamo di avere al tempo $ t = 0 $ un campo elettrico che possiamo scrivere in trasformata
	\[
        E(x, 0) = f(x) = \int_{-\infty}^{+\infty} A(k) e^{ikx} \dif{k}.
	\]
	dove $ A(k) $ è una funzione che fornisce nell'integrale contributi non trascurabili solo in un certo intervallo $ \abs{k - k_0} < \Delta k $ con $ \Delta k \ll k_0 $. Dall'equazione d'onda sappiamo che ogni componente monocromatica del campo si propaga con frequenza fissata dalla relazione di dispersione
	\[
	k^2 = \frac{\omega^2}{c^2} \epsilon(\omega) \quad \Rightarrow \quad \omega = \omega(k)
	\]
	dove in questo caso $ \epsilon $ è reale ma non possiamo trascurarne la dipendenza dalla frequenza. La relazione di dispersione permette di determinare l'evoluzione temporale del pacchetto
	\[
	E(x, t) = \int_{-\infty}^{+\infty} A(k) e^{i(kx - \omega(k) t)} \dif{k} \simeq \int_{k_0 - \Delta k}^{k_0 + \Delta k} A(k) e^{i(kx - \omega(k) t)} \dif{k}
	\]
	dove abbiamo usato il fatto che il pacchetto è localizzato $ k $ vicino a $ k_0 $. Essendo $ \Delta k \ll k_0 $ possiamo sviluppare al primo ordine in $ k - k_0 $ la dipendenza da $ k $ di $ \omega $:
	\[
	\omega(k) \simeq \omega(k_0) + \od{\omega}{k}(k_0) (k - k_0).
	\]
	Pertanto
	\begin{align*}
	E(x, t) & \simeq \int_{k_0 - \Delta k}^{k_0 + \Delta k} A(k) \exp\left[ikx - i \omega(k_0)t - i \dod{\omega}{k}(k_0) (k - k_0)t \right] \dif{k} \\
	& = \exp\left(ik_0 \dod{\omega}{k}(k_0)t - i \omega(k_0) t\right) \int_{k_0 - \Delta k}^{k_0 + \Delta k} A(k) \exp\left[ik\left(x - \dod{\omega}{k}(k_0) t\right)\right] \dif{k} \\
	& \simeq f\left(x - \dod{\omega}{k}(k_0) t\right) e^{i\left(k_0 \od{\omega}{k}(k_0) - i \omega(k_0)\right)t}.
	\end{align*}
	Ora ci interessa vedere l'energia: supponendo che il campo iniziale fosse reale abbiamo
	\[
	\Re{\left\{E E^{*}\right\}} = \left[f\left(x - \dod{\omega}{k}(k_0) t\right)\right]^2 \propto I\left(x - \dod{\omega}{k}(k_0) t\right).
	\]
	Pertanto la velocità effettiva del pacchetto non è $ \omega/k $ ma $ \od{\omega}{k} (k_0) $, che viene chiamata velocità di gruppo $ v_g $.


	A questo punto ci piacerebbe scrivere una conservazione dell'energia locale del tipo
	\[
	- \od{}{t}\left(\int_V u \; \mathrm{d}^3 r \right) = \int_V \vec{J} \cdot \vec{E} + \oint_{\partial V} \vec{S} \cdot \dif{\vec{a}} \quad \Rightarrow \quad \pd{u}{t} + \div {\vec S} + \vec{J} \cdot \vec{E} = 0.
	\]
	Con gli stessi passaggi formali del caso microscopico si arriva all'espressione
	\[
	\vec{J} \cdot \vec{E} = - \div{\left(\frac{c}{4\pi} \vec{E} \times \vec{H}\right)} - \frac{1}{4\pi} \left(\vec{E} \cdot \dpd{\vec{D}}{t} + \vec{H} \cdot \dpd{\vec{B}}{t}\right).
	\]

	Interpretiamo il primo termine come la divergenza del vettore di Poynting
	\[
	\vec S = \frac{c}{4\pi} \vec E \times \vec H
	\]
	osservando che tale espressione è valida indipendentemente dalle caratteristiche del mezzo (ovvero dalla particolar forma di $ \epsilon(\omega) $) in quanto è l'unica espressione compatibile con quella nel vuoto a causa delle condizioni di raccordo. Considerando infatti un'interfaccia tra il vuoto ed un mezzo materiale in assenza di sorgenti libere, la componente ortogonale del vettore di Poynting all'interfaccia deve essere necessariamente continua: il campo $ \vec{E}_{\parallel} $ e il campo $ \vec{H}_{\parallel} $ sono continui e fuori dal materiale vale $ \vec{S}_{\perp} = c/4\pi \vec{E}_{\parallel} \times \vec{B}_{\parallel} = c/4\pi \vec{E}_{\parallel} \times \vec{H}_{\parallel} $ quindi $ \vec{S} = c/4\pi \vec{E} \times \vec{H} $ dà la corretta espressione.

	Per quanto riguarda la densità di energia $ u $ non possiamo introdurre una formula analoga a quella data nel caso di mezzi trasparenti e non dispersivi $ u = (\vec{E} \cdot \vec{D} + \vec{B} \cdot \vec{H})/8\pi $, essendo in generale $ \vec{E} \cdot \pd{\vec{D}}{t} \neq \frac{1}{2} \pd{}{t}(\vec{E} \cdot \vec{D}) $ se $ \epsilon = \epsilon(\omega) $ (la riposta del mezzo non è locale nel tempo). Tuttavia nel caso in cui l'assorbimento sia trascurabile è possibile introdurre una densità di energia per un pacchetto d'onda del tipo $ \vec{E} = \vec{E}_0(\vec{r}, t) e^{-i\omega t} $ dove $ \vec{E}_0(\vec{r}, t) $ è una funzione che varia poco sulla scala temporale di $ 1/\omega $: l'energia è data dalla formula di Brillouin
	\[
	u = \frac{1}{8\pi} \left(\dod{(\omega \epsilon(\omega))}{\omega} E^2  + \dod{(\omega \mu(\omega))}{\omega} H^2\right) = \frac{1}{8\pi} \frac{E^2}{\omega \mu(\omega)} \od{}{\omega}\left(\omega^2 \epsilon(\omega) \mu(\omega)\right).
	\]
	Per convincerci della validità di quest'espressione vediamo cosa succede in media temporale sulla scala di tempi delle rapide variazioni: posto $ E^2 = \langle \vec{E}_0^2 \rangle $ e similmente per $ \vec{H} $ si ha
	\[
	\langle u \rangle = \frac{E^2}{16 \pi \omega \mu} \od{}{\omega}\left(c^2 k^2\right) = \frac{E^2}{16 \pi \omega \mu} c^2 2k \od{k}{\omega} = \frac{c n E^2}{8 \pi \mu} \od{k}{\omega}
	\]
	dove abbiamo usato la relazione di dispersione $ k^2 = \omega^2/c^2 \epsilon(\omega) \mu(\omega) $ e la relazione $ B = n(\omega) E $.  La media del vettore di Poynting è invece
	\[
	\langle S \rangle = \frac{c}{8\pi} EH = \frac{c}{8\pi} \frac{nE^2}{\mu}.
	\]
	È quindi soddisfatta la relazione $ \langle S_z \rangle = \langle u \rangle v_g $ che è quanto aspettavamo dato che in un mezzo dispersivo il trasporto di energia di un pacchetto d'onda avviene alla velocità di gruppo.

	Un osservazione interessante è che nel modello di Drude--Lorentz la regione di grandi $\omega$, in cui $\epsilon$  è leggermente meno di 1, non può essere considerata una zona di tipo I in quanto la velocità di fase è $ \omega/k = c/n > c $, ma deve essere considerata di tipo II. Si trova infatti che la velocità di gruppi è $ v_g = c n < c $ come volevamo.
\end{proof}

\begin{dissertazione} \label{diss:polaritoni}
    Plasmoni e polaritoni.
\end{dissertazione}
\begin{proof}
    I plasmoni e i polaritoni rappresentano i modi normali di propagazione di un'onda elettromagnetica nella materia, risultanti dalla combinazione delle oscillazioni proprie del mezzo materiale con quelle della radiazione. Consideriamo il modello di Drude--Lorentz nel limite di assorbimento trascurabile $ \gamma \ll \omega_0 $: omettendo la parte immaginaria $ i \gamma \delta(\omega - \omega_0) $, la funzione dielettrica è
    \[
        \epsilon(\omega) = 1 + \frac{\omega_p^2}{\omega_0^2 - \omega^2} = \frac{\omega_L^2 - \omega^2}{\omega_0^2 - \omega^2}
    \]
    dove abbiamo definito $ \omega_L^2 \coloneqq \omega_p^2 + \omega_0^2 $. Cerchiamo soluzioni delle equazioni di Maxwell in assenza di sorgenti esterne lavorando in componenti monocromatiche
    \[
        \div{\vec{D}} = 0 \quad \div{\vec{B}} = 0 \quad \rot{\vec{E}} = \frac{i\omega}{c} \vec{B} \quad \rot{\vec{B}} = - \frac{i\omega}{c} \vec{D}
    \]
    Prendendo il rotore della terza equazione si ricava
    \[
        \rot{\left(\rot{\vec{E}}\right)} = \nabla{\left(\div{\vec{E}}\right)} - \nabla^2{\vec{E}} = \frac{i\omega}{c} \rot{\vec{B}} = \frac{\omega^2}{c^2} \vec{D}
    \]
    Ora in componenti monocromatiche vale $ \vec{D} = \epsilon(\omega) \vec{E} $ pertanto $ \div{\vec{D}} = \epsilon(\omega) \div{\vec{E}} = 0 $. Dobbiamo quindi distinguere due casi in base al fatto che la costante dielettrica sia nulla o meno alla frequenza considerata. Nel modello considerato questo accade solo per la frequenza longitudinale $ \omega_L $.

    Se $ \epsilon(\omega) = 0 $ allora $ \vec{D} = 0 $ e il campo elettrico soddisfa l'equazione $ \nabla{\left(\div{\vec{E}}\right)} = \nabla^2{\vec{E}} $. Cercando soluzioni di tipo onda piana $ \vec{E} = \varvec{\mathcal{E}} e^{i \vec{k} \cdot \vec{r} - \omega t} $ otteniamo
    \[
        \abs{\vec{k}}^2 \varvec{\mathcal{E}} = \vec{k} (\vec{k} \cdot \varvec{\mathcal{E}})
    \]
    che è soddisfatta se e solo se $ \varvec{\mathcal{E}} = \mathcal{E} \hat{\vec{k}} $, ovvero se l'onda piana è un'onda puramente longitudinale. Si verifica che le equazioni di Maxwell sono soddisfatte ponendo $ \vec{E} = \mathcal{E} \hat{\vec{k}} e^{i \vec{k} \cdot \vec{r} - \omega t} $ e $ \vec{D} = \vec{B} = \vec{H} = 0 $. Osserviamo che mentre la frequenza dell'onda è fissata dalla relazione $ \epsilon(\omega) = 0 $, il vettore d'onda $ \vec{k} $ non ha vincoli. Tale soluzione viene detta \emph{campo plasmonico}.

    Se consideriamo invece il caso $ \epsilon(\omega) \neq 0 $ si ha $ \div{\vec{E}} = 0 $ e pertanto il campo elettrico soddisfa l'equazione d'onda
    \[
        \nabla^2 \vec{E} + \frac{\omega^2}{c^2} \epsilon(\omega) \vec{E} = 0.
    \]
    Cercando soluzioni di tipo onda piana $ \vec{E} = \varvec{\mathcal{E}} e^{i(\vec{k} \cdot \vec{r} - \omega t)} $ un primo vincolo viene dall'equazione sulla divergenza che impone $ \vec{k} \cdot \varvec{\mathcal{E}} = 0 $ mentre l'equazione d'onda fornisce la seguente relazione di dispersione
    \[
        \abs{\vec{k}}^2 = \frac{\omega^2}{c^2} \epsilon(\omega) = \frac{\omega^2}{c^2} \frac{\omega_L^2 - \omega^2}{\omega_0^2 - \omega^2}.
    \]
    Il campo risultante è quindi trasversale con modulo del vettore d'onda fissato una volta fissata la frequenza. Tale soluzione viene detta \emph{campo polaritonico}.

    In Figura \ref{fig:polaritoni} riportiamo il grafico dei possibili valori di $ \omega^2 $ in funzione del modulo quadro del vettore d'onda $ k^2 $. La radiazione ha due modi propri di oscillazione in quanto nel vuoto il campo elettrico può oscillare solo trasversalmente alla direzione di propagazione. La materia, nel caso in cui sia isotropa (come considerato nel modello di Drude--Lorentz), ha 3 modi propri di oscillazioni. Ci aspettiamo quindi di avere 5 soluzioni per $ \omega $ per ogni valore di $ k $.

    I due asintoti $ k^2 = \omega_0^2 $ e $ k^2 = c^2 \omega^2 $ rappresentano i modi propri del sistema: quello obliquo rappresenta la radiazione da sola, mentre quello orizzontale è il modo proprio del materiale. Come ci si poteva aspettare, ad altissimi valori di $ k $ i due modi non si mischiano e otteniamo essenzialmente una cosa molto simile ad uno solo dei due modi, mentre se ci avviciniamo al valore $ k^2 = \omega_0^2/c^2 $ per cui gli asintoti si incrociano otteniamo il fenomeno dell'\emph{anticrossing}, ovvero i due modi si mischiano e non otteniamo più un incrocio. Notiamo inoltre che la distanza in $ \omega $ fra i due modi di oscillazione mischiati (ovvero per $ k^2 = \omega_0^2/c^2 $) è proprio $ \omega_p $ al primo ordine in $ \omega_p/\omega_0 $.

    Per quanto riguarda il discorso sul numero di modi normali, due arrivano dalla parte inferiore del grafico (sono due perché l'onda è trasversale e quindi ci sono due modi di oscillazione, il modo normale è degenere), e viene chiamato \emph{polaritone inferiore}. Altri due ci arrivano dalla parte superiore del grafico e viene chiamato \emph{polaritone superiore}. La quinta soluzione arriva proprio dal \emph{plasmone}, orizzontale e presente ad $ \omega_L $, per ogni $ k $.

    \begin{figure}[h]
        \centering
        \begin{tikzpicture}

        \pgfmathsetmacro{\xlim}{20}
        \pgfmathsetmacro{\wo}{1.0}
        \pgfmathsetmacro{\wl}{1.2}
        \pgfmathsetmacro{\c}{1.0}

        \begin{axis}[
        width=1\textwidth,
        height=1.5*\axisdefaultheight,
        axis lines=center,
        xmin=-1, xmax=\xlim,
        xlabel=$ k^2 $,
        xlabel style={below},
        ymin=-0.3, ymax=4,
        ylabel=$ \omega^2 $,
        ylabel style={left},
        samples=200,
        ytick=\empty,
        xtick=\empty,
        extra x ticks={\wo^2/\c^2},
        extra x tick labels={$ \omega_0^2/c^2 $},
        extra y ticks={\wo^2, \wl^2},
        extra y tick labels={$ \omega_0^2 $, $ \omega_L^2 $}
        ]
        \addplot[mark=none, domain=0:\xlim] {\wl^2};
        \addplot[mark=none, domain=0:\xlim] {0.5*(\wl^2 + \c^2*\x + sqrt((\wl^2 + \c^2 * \x)^2 - 4*\c^2*\wo^2*\x))};
        \addplot[mark=none, domain=0:\xlim] {0.5*(\wl^2 + \c^2*\x - sqrt((\wl^2 + \c^2 * \x)^2 - 4*\c^2*\wo^2*\x))};
        \addplot[mark=none, domain=0:\xlim, dashed, blue] {\wo^2};
        \addplot[mark=none, domain=0:\xlim, dashed, blue] {\x * \c^2};
        \addplot[mark=none, domain=0:\xlim, dashed] {(\wo^2 * \c^2) / \wl^2 * \x};
        \end{axis}

        \end{tikzpicture}
        \caption{plasmoni e polaritoni.}
        \label{fig:polaritoni}
    \end{figure}

\end{proof}

\begin{dissertazione} \label{diss:kk}
    Relazioni di Kramers--Kronig e regole di somma.
\end{dissertazione}
\begin{proof}
    La più generale relazione lineare fra $ \vec{E} $ e $ \vec{P} $ è del tipo
    \[
        P_i(\vec{r}, t) = \int_{\R^3} \mathrm{d}^3 r' \int_{-\infty}^{+\infty} \dif{t} \; g_{ij}(\vec{r}, \vec{r}', t, t') E_{j}(\vec{r}', t').
    \]
    Nel caso in cui si considerino risposte locali nello spazio e materiali omogenei, isotropi e stazionari, la relazione precedente si riduce a
    \[
        \vec{P}(\vec{r}, t) = \int_{-\infty}^{+\infty} \dif{t} \; g(t - t') \vec{E}(\vec{r}', t').
    \]
    Ora il principio di causalità richiede che la risposta del mezzo al tempo $ t $ possa dipendere solo da quello che è successo prima del tempo $ t $: per questo motivo la funzione $ g(\tau) $ sia identicamente nulla per $ \tau < 0 $. Passando in trasformata la relazione lineare precedente si riduce a una relazione di proporzionalità
    \[
        P(\omega) = \chi(\omega) E(\omega)
    \]
    dove abbiamo indicato con $ \chi $ la trasformata di $ g $ e abbiamo omesso la notazione vettoriale essendo $ \vec{P} $ e $ \vec{E} $ paralleli. Ora la funzione $ \chi $ è in generale una funzione complessa $ \chi = \chi_1 + i \chi_2 $ di variabile reale; tuttavia essendo $ g $ reale questo impone che si abbia
    \[
        (\chi(\omega))^{*} = \chi(-\omega) \quad \Rightarrow \quad
        \begin{cases*}
            \chi_1(\omega) = \chi_1(- \omega) \\
            \chi_2(\omega) = - \chi_2(\omega)
        \end{cases*}
    \]
    Veniamo ora alle relazioni di Kramers--Kronig. Il fatto che $ g(\tau) $ sia nulla per $ \tau < 0 $ ci permette di estendere in modo analitico la funzione $ \chi(\omega) $ di variabile reale a una funzione complessa di variabile complessa $ \chi(\tilde{\omega}) $ con $ \tilde{\omega} = \omega_1 + i \omega_2 $ nel semipiano superiore complesso $ \omega_2 \geq 0 $. Fissiamo un punto $ \omega' $ sull'asse reale del piano complesso e il cammino $ \Gamma $ costituito da un semicerchio $ S_R $ di raggio $ R $ centrato nell'origine contenuto nel semipiano superiore complesso, chiuso sull'asse reale da una linea retta che congiunge $ \omega_1 = -R $ a $ \omega_1 = R $ che evita con un semicerchio $ S_\epsilon $ di raggio $ \epsilon $ il punto $ \omega_1 = \omega $ (sempre contenuto nel semipiano superiore complesso). Essendo $ \frac{\chi(\tilde{\omega})}{\omega' - \tilde{\omega}} $ analitica all'interno di tale cammino abbiamo per il teorema di Cauchy-Goursat
    \[
        0 = \oint_{\Gamma} \frac{\chi(\tilde{\omega}')}{\omega - \tilde{\omega}'} \dif{\tilde{\omega}'} = \int_{S_R} + \int_{-R}^{\omega' - \epsilon} + \int_{\omega' + \epsilon}^{R} - \int_{\epsilon}.
    \]
    Mandando $ R \to +\infty $ l'integrale su $ S_R $ si annulla perché scala come $ 1/R $; l'integrale su $ S_\epsilon $ sarà pari alla metà del residuo nel punto. Mandando $ \epsilon \to 0 $ l'integrale sull'asse reale si riduce ad un integrale in parte principale di Cauchy. Otteniamo quindi
    \[
        i \pi \chi(\omega) = \mathcal{P}\!\int_{-\infty}^{+\infty} \frac{\chi(\omega')}{\omega - \omega'} \dif{\omega'}
    \]
    dove abbiamo omesso la ``tilde'' essendo ora l'integrale fatto sull'asse reale. Separando la parte reale e immaginaria otteniamo le \emph{trasformate di Hilbert}
    \[
        \chi_1(\omega) = \frac{1}{\pi} \, \mathcal{P}\!\int_{-\infty}^{+\infty} \frac{\chi_2(\omega')}{\omega - \omega'} \dif{\omega'}
        \qquad
        \chi_2(\omega) = - \frac{1}{\pi} \, \mathcal{P}\!\int_{-\infty}^{+\infty} \frac{\chi_1(\omega')}{\omega - \omega'} \dif{\omega'}
    \]
    Possiamo ora usare le simmetrie di partirà e disparità che abbiamo dedotto sulla parte reale e immaginaria di $ \chi $ per ``ripiegare'' gli integrali sul semiasse positivo (fisico) delle frequenze
    \[
        \chi_1(\omega) = \frac{2}{\pi} \, \mathcal{P}\!\int_{0}^{\infty} \frac{\omega' \chi_2(\omega')}{\omega'^2 - \omega^2} \dif{\omega'}
        \qquad
        \chi_2(\omega) = -\frac{2\omega}{\pi} \, \mathcal{P}\!\int_{0}^{\infty} \frac{\chi_1(\omega')}{\omega'^2 - \omega^2} \dif{\omega'}.
    \]
    Usando ora la costante dielettrica $ \epsilon = 1 + 4 \pi \chi = \epsilon_1 + i \epsilon_2 $ otteniamo le relazioni di Kramers-Kronig
    \[
        \epsilon_1(\omega) - 1 = \frac{2}{\pi} \, \mathcal{P}\!\int_{0}^{\infty} \frac{\omega' \epsilon_2(\omega')}{\omega'^2 - \omega^2} \dif{\omega'}
        \qquad
        \epsilon_2(\omega) = -\frac{2\omega}{\pi} \, \mathcal{P}\!\int_{0}^{\infty} \frac{\epsilon_1(\omega') - 1}{\omega'^2 - \omega^2} \dif{\omega'}
    \]
    Da tali relazioni possiamo per esempio ricavare che la costante dielettrica statica è
    \[
	    \epsilon_1(0) - 1 = \frac{2}{\pi} \, \mathcal{P}\!\int_{0}^{\infty} \frac{\epsilon_2(\omega')}{\omega'} \dif{\omega'}
    \]
    da cui otteniamo che deve essere $ \epsilon(0) > 1 $ essendo la $ \epsilon_2 $ positiva per ogni frequenza (l'energia dissipata associata alla propagazione di onde piane omogenee è proporzionale alla $ \epsilon_2 $ e stiamo considerando sistemi all'equilibrio termodinamico).

    Ricordiamo però che nei metalli accade qualcosa di leggermente diverso, in quanto la conducibilità statica $ \sigma $ aggiunge una singolarità (polo per la funzione analitica) in $ \omega = 0 $. La seconda delle trasformate di Hilbert va quindi modificata. Indicando con $ \chi'(\omega) = \chi(\omega) + i \frac{\sigma}{\omega} $ abbiamo che
    \begin{align*}
	    \mathcal{P}\!\int_{-\infty}^{+\infty} \frac{\chi'(\omega')}{\omega - \omega'} \dif{\omega'} & = \mathcal{P}\!\int_{-\infty}^{+\infty} \frac{\chi(\omega')}{\omega - \omega'} \dif{\omega'} +
	    i \mathcal{P}\!\int_{-\infty}^{+\infty} \frac{\sigma}{\omega'(\omega - \omega')} \dif{\omega'} \\
	    & = i \pi \chi(\omega) + i \mathcal{P}\!\int_{-\infty}^{+\infty} \frac{\sigma}{\omega'(\omega - \omega')} \dif{\omega'}
    \end{align*}
    Per $ \omega \neq 0 $ si mostra che il secondo integrale è nullo mentre per $ \omega = 0 $ diverge: si ha quindi per $ \omega \neq 0 $
    \[
	    \mathcal{P}\!\int_{-\infty}^{+\infty} \frac{\chi'(\omega')}{\omega - \omega'} \dif{\omega'} = i \pi \chi(\omega) = i \pi \chi'(\omega) - \frac{\sigma}{\omega}
    \]
    da cui
    \[
        \chi'(\omega) = - \frac{i}{\pi} \, \mathcal{P}\!\int_{-\infty}^{+\infty} \frac{\chi'(\omega')}{\omega - \omega'} \dif{\omega'} + i\frac{\sigma}{\omega}
    \]
    Integrando per parti l'espressione di $ \chi(\omega) $ i termini della sua trasformata otteniamo
    \[
        \chi(\omega) = i \frac{g(0^{+})}{\omega} - \frac{g'(0^{+})}{\omega^{2}} + i \frac{g''(0^{+})}{i\omega^3} + O\left(\frac{1}{\omega^4}\right).
    \]
    Il Jackson afferma che $ g(0^{+}) $ è nulla per continuità con $ g(0^{-}) $ tranne per i metalli. Con questo assunto ricaviamo gli svipuppi asintotici per $ \omega \to +\infty $: $ \chi_1(\omega) \sim 1/\omega^2 $, mentre $ \chi_2(\omega) \sim 1/\omega $ per i metalli e $ \chi_2(\omega) \sim 1/\omega^3 $ in assenza di conducibilità.

    Tramite questi sviluppi possiamo ricavare le regole di somma. Definendo
    \[
        \omega_p^2 = \lim_{\omega \to +\infty} \omega^2\left(1 - \epsilon_1(\omega)\right)
    \]
    dalla prima regola di Kramers--Kronig con $ \omega \to +\infty $ si ha
    \[
        - \frac{\omega_p^2}{\omega^2} = \frac{2}{\pi} \, \mathcal{P}\! \int_{0}^{+\infty} \frac{\omega' \epsilon_2(\omega')}{-\omega^2} \dif{\omega'} \quad \Rightarrow \quad \int_{0}^{+\infty} \omega' \epsilon_2(\omega') \dif{\omega'} = \frac{\pi}{2} \omega_p^2
    \]
    che è detta regola di somma sulla forza di oscillatore. Nel modello di Drude--Lorentz tale integrale deve scalare con la densità elettronica $ n $ dato che $ \omega_p^2 \propto n $. Definendo invece in assenza di conducibilità
    \[
        \gamma \omega_p^2 = \lim_{\omega \to +\infty} \omega^3 \epsilon_2(\omega)
    \]
    e sviluppando la seconda Kramers--Kronig per $ \omega \to +\infty $ si ha
    \[
        \frac{\gamma\omega_p^2}{\omega^3} = \frac{4\pi \sigma}{\omega} - \frac{2\omega}{\pi} \, \mathcal{P}\!\int_{0}^{\infty} \frac{\epsilon_1(\omega') - 1}{-\omega^2} \dif{\omega'} \quad \Rightarrow \quad \int_{0}^{\infty} (\epsilon_1(\omega') - 1) \dif{\omega'} = - 2 \pi^2 \sigma + O\left(\frac{1}{\omega^2}\right)
    \]
    che è quindi nullo in assenza di conducibilità. Tale regola è detta ``DC conductivity sum rule''.
\end{proof}

\begin{dissertazione} \label{diss:nonlin}
    Suscettibilità non lineari (del secondo e terzo ordine).
\end{dissertazione}
\begin{proof}
    In generale la risposta di un mezzo a campi elettromagnetici esterni può essere non lineare. La presenza di non linearità si manifesta sicuramente quanto il campo esterno è abbastanza intenso da poter ionizzare la materia; una stima rozza di tale soglia è $ E_0 \sim e/a_0^2 \sim 10^9 \; \mathrm{V / cm} $ dove $ a_0 $ è il raggio atomico. Il modello più semplice di polarizzazione non lineare consiste nel supporre che la polarizzazione ammetta un'espansione in serie del tipo
    \[
        P_i = \chi^{(1)}_{ij}E_j + \chi^{(2)}_{ijk} E_j E_k + \chi^{(3)}_{ijkl} E_j E_k E_l + \cdots
    \]
    dove di norma il termine di ordine $ k $ è ridotto di un fattore $ E_0 $ rispetto al termine di ordine $ k-1 $.

    Osserviamo prima di tutto che essendo $ \vec{P} $ ed $ \vec{E} $ vettori essi cambiano di segno sotto parità. Se il sistema è simmetrico sotto tale trasformazione la $ \chi^{(2)} $ è invariata: di conseguenza possiamo affermare che il termine di secondo ordine si annulla identicamente. È per questo motivo che in molti materiali gli effetti del il termine $ \chi^{(2)} $ (e in generale tutti i termini di ordine pari) risultano trascurabili.

    Nonostante la non linearità può essere comunque comodo continuare a lavorare in componenti monocromatiche. Tuttavia, a differenza del caso lineare in cui una volta passati in trasformata potevamo trattare le varie frequenze in modo indipendente, se si considerano effetti non lineari questa separazione non è più possibile. Se infatti consideriamo due campi della forma $ E_i = \mathcal{E}_i e^{-i\omega_1 t} + \mathcal{E}_i^{*} e^{i\omega_1 t} $ e $ E_i = \mathcal{E}_i e^{-i \omega_2 t} + \mathcal{E}_i^{*} e^{i \omega_2 t} $ il termine del secondo ordine genera una polarizzazione alle frequenze $ \omega_1 \pm \omega_2 $. $ P_i^{(2)} $ conterrà infatti un termine della forma
    \begin{gather*}
        \chi_{ijk}^{(2)} \left(\mathcal{E}_j e^{-i\omega_1 t} + \mathcal{E}_j^{*} e^{i\omega_1 t}\right)\left(\mathcal{E}_k e^{-i \omega_2 t} + \mathcal{E}_k^{*} e^{i \omega_2 t}\right) = \\
        = \chi_{ijk}^{(2)} \mathcal{E}_j \mathcal{E}_k e^{-i(\omega_1 + \omega_2)t} + \chi_{ijk}^{(2)} \mathcal{E}_j^{*} \mathcal{E}_k^{*} e^{i(\omega_1 + \omega_2)t} + \chi_{ijk}^{(2)} \mathcal{E}_j \mathcal{E}_k^{*} e^{-i(\omega_1 - \omega_2)t} + \chi_{ijk}^{(2)} \mathcal{E}_j \mathcal{E}_k^{*} e^{i(\omega_1 - \omega_2)t}.
    \end{gather*}
    In particolare nel caso $ \omega_1 = \omega_2 = \omega $ compare un termine con frequenza doppia e un termine statico: questi fenomeni vengono chiamati rispettivamente \emph{SHG} (second harmonic generation) e \emph{rettificazione}. In maniera analoga considerando tre campi a frequenze $ \omega_1 $, $ \omega_2 $ e $ \omega_3 $, il termine di terzo ordine genera polarizzazioni con frequenza $ \omega_1 \pm \omega_2 \pm \omega_3 $; in particolare se le frequenze sono tutte uguali a $ \omega $ il termine di terzo ordine genera onde con frequenze pari a quella dei campi $ \omega $ e con frequenza $ 3\omega $ e si parla di \emph{THG} (third harmonic generation). Abbiamo quindi otteuto risposte del mezzo a frequenze diverse da quella della forzante, cosa mai vista nei fenomeni lineari.

    Considerando ora il caso in cui la $ \chi^{(2)} $ sia trascurabile, osserviamo che la componente alla stessa frequenza della forzante generata dalla terzo ordine è particolarmente importante dato che in tale caso la polarizzazione assume la forma
    \[
        P_i = \chi_{ij}^{(1)}E_j + \beta I \xi_{ij}^{(3)} E_j
    \]
    essendo nel termine di terzo ordine termini proporzionali a $ \chi_{ijkl}^{(3)} \mathcal{E}_k \mathcal{E}_l^{*} $ che è proporzionale all'intensità $ I $ dell'onda. Di conseguenza è possibile introdurre un ``indice di rifrazione'' che dipende dall'intensità dell'onda. Questo fenomeno si chiama effetto Kerr ottico. La suscettibilità può essere positiva o negativa. Nel caso sia positiva si parla di \emph{self-focusing}, ovvero mandando un pacchetto d'onda che ha intensità disomogenea, questo andrà a restringersi.

    Questi fenomeni si possono vedere anche semplicemente andando a modificare il modello microscopico di Drude--Lorentz aggiungendo perturbazioni al potenziale armonico del tipo
    \[
        V(x) = \frac{1}{2} m \omega_0^2 x^2 + \frac{1}{3} m \beta x^3 + \frac{1}{4} m \delta x^4 + \cdots
    \]
    e cercando soluzioni perturbative alla soluzione classica.
\end{proof}

\begin{dissertazione} \label{diss:locale}
    Effetti di campo locale.
\end{dissertazione}
\begin{proof}
    Per stati della materia come quello gassoso, in cui le molecole sono lontane, possiamo supporre che in presenza di un campo esterno gli atomi acquistino un dipolo $ \vec{p} = \alpha \vec{E} $ dove $ \vec{E} $ è il campo macroscopico e $ \alpha $ è la polarizzabilità atomica. Da tale relazione abbiamo ricavato la costante dielettrica $ \epsilon = 1 + 4\pi n \alpha $ dove $ n $ è la densità volumica del gas. In altri termini stiamo supponendo di poter trascurare gli effetti del campo elettrico generato dai dipoli indotti dal campo sui dipoli stessi: infatti in generale l'espressione corretta da utilizzare è $ \vec{p} = \alpha \vec{E}\ped{loc} $ dove $ \vec{E}\ped{loc} $ è il campo locale sull'atomo che in generale è diverso dal campo macroscopico $ \vec{E} $.

    Possiamo stimare gli effetti del campo locale in un caso di altissima simmetria, ovvero di un reticolo cubico infinito di passo reticolare $ a $ formato da atomi identici di polarizzabilità $ \alpha $. Il campo esterno è un campo macroscopico nel senso che varia su una scala $ \lambda \gg a $. Consideriamo un atomo posto nell'origine e una sfera di raggio $ R $ centrata su di esso con raggio mesoscopico $ \lambda \gg R \gg a $. Essendo $ R $ mesoscopico possiamo affermare che il campo dovuto agli atomi polarizzati fuori dalla sfera sia ben descritto dal campo $ \vec{E} $. Inoltre, sempre grazie alla scala scelta per $ R $, possiamo affermare che il campo macroscopico dovuto alla sfera è quello di una sfera dielettrica i campo uniforme pari a $ \vec{E}\ped{sfera} = -\frac{4\pi}{3} \vec{P} $ dove $ \vec{P} $ è il vettore polarizzazione. Il campo al centro della sfera sarà quindi
    \[
        \vec{E}\ped{loc} = \vec{E} - \vec{E}\ped{sfera} + \vec{E}\ped{micro}
    \]
    dove $ \vec{E}\ped{micro} $ è il campo microscopico prodotto dai dipoli nella sfera di raggio $ R $. Essendo il campo $ \vec{E} $ costante e uniforme nella sfera i dipoli indotti saranno tutti uguali $ \vec{p} = p_x\hat{\vec{x}} + p_y\hat{\vec{y}}  + p_z\hat{\vec{z}} $ e produrranno un campo di dipolo
    \[
        \vec{E}\ped{d} = \frac{3 (\vec{p} \cdot \vec{r}) \vec{r} - \vec{p} r^2}{r^5}.
    \]
    Indicizzando la posizione degli atomi con $ \vec{r} = i a\hat{\vec{x}} + i a\hat{\vec{y}}  + k a\hat{\vec{z}} $ con $ i $, $ j $ e $ k $ interi tali che $ i^2 + j^2 + k^2 \leq R^2 $ abbiamo che \emph{wlog} la componente $ x $ del campo microscopico è
    \[
        \vec{E}\ped{micro} \cdot \hat{\vec{x}} = \sum_{i, j, k} \frac{3ia(ia p_x + ja p_y + ka p_z) - p_x a^2 (i^2+j^2+k^2)}{(i^2+j^2+k^2)^{\frac{5}{2}}a^5}.
    \]
    In questa somma abbiamo termini di due tipi: i termini misti della forma
    \[
        \sum_{i, j, k} \frac{ij}{(i^2+j^2+k^2)^{\frac{5}{2}}} \qquad \sum_{i, j, k} \frac{ik}{(i^2+j^2+k^2)^{\frac{5}{2}}}
    \]
    che sono nulli in quanto nella somma dentro la sfera i termini della somma compaiono singolarmente 2 volte con segno opposto; i termini diagonali della forma
    \[
        \sum_{i, j, k} \frac{i^2}{(i^2+j^2+k^2)^{\frac{5}{2}}} \qquad  \sum_{i, j, k} \frac{j^2}{(i^2+j^2+k^2)^{\frac{5}{2}}} \qquad  \sum_{i, j, k} \frac{k^2}{(i^2+j^2+k^2)^{\frac{5}{2}}}
    \]
    che si elidono in quanto sono somme tutte uguali, ma compare un volta positiva con un 3 davanti e 3 volte con un $ -1 $ davanti. Data la simmetria del sistema concludiamo quindi che $ \vec{E}\ped{micro} = 0 $.

    Quindi il campo locale sull'atomo centrale è dato da
    \[
        \vec{E}\ped{loc} = \vec{E} + \frac{4\pi}{3}\vec{P}.
    \]
    Il vettore polarizzazione è $ \vec{P} = n \alpha \vec{E}\ped{loc} $ da cui sostituendo il campo locale si ottiene
    \[
        \vec{P} = \frac{n \alpha}{1 - \dfrac{4\pi}{3} n \alpha} \vec{E} = \chi \vec{E}.
    \]
    Infine la costante dielettrica è data da
    \[
        \epsilon = 1 + 4 \pi \chi = 1 + \frac{4\pi n \alpha}{1 - \dfrac{4\pi}{3} n \alpha}.
    \]
    Osserviamo che la $ \epsilon \simeq 1 + 4\pi n \alpha $ per $ n \to 0 $ come ottenuto per un gas. Inoltre nel in questo caso vale $ \epsilon $ è maggiore del caso del gas, ovvero gli atomi tendono a rafforzare a vicenda la polarizzazione. Possiamo riscrivere la relazione precedente per ottenere la \emph{relazione di Clausius-Mossotti} che lega la quantità macroscopica $ \epsilon $ con quelle microscopiche $ n $ e $ \alpha $
    \[
        \frac{\epsilon - 1}{\epsilon + 2} = \frac{4\pi}{3} n \alpha.
    \]

    Per finire possiamo far vedere che gli effetti di campo locale non apportano modifiche sostanziali al modello di Drude--Lorentz (per il quale abbiamo approssimato il campo locale con quello macroscopico). In tale modello il corrispondente di $ n \alpha $ è $ \omega_p^2/(4\pi(\omega_0^2 - i \gamma \omega - \omega^2)) $ quindi
    \[
        \vec{P} = \frac{\frac{\omega_p^2/4\pi}{\omega_0^2 - i \gamma \omega - \omega^2}}{1 - \frac{4\pi}{3}\frac{\omega_p^2/4\pi}{\omega_0^2 - i \gamma \omega - \omega^2}} \vec{E} = \frac{\omega_p^2/4\pi}{\omega_0^2 - \frac{\omega_p^2}{3} - i \gamma \omega - \omega^2} \vec{E}
    \]
    che è un'espressione uguale in forma a quella ottenuta trascurando il gli effetti campo locale con la sostituzione $ \omega_0^2 \to \omega_0^2 - \omega_p^2/3 $. Per il nostro modello che è puramente fenomenologico, i parametri in gioco non sono dati in termini di costanti fondamentali ma sono solo parametri di \emph{fit} e pertanto non ci possiamo accorgere di questa differenza.
\end{proof}

\begin{dissertazione} \label{diss:gaspolare}
	Costante dielettrica statica di un gas ideale di molecole polari.
\end{dissertazione}
\begin{proof}
	Consideriamo un gas di molecole dotate di un momento di dipolo permanente $ \vec{p} $ immerso in un campo esterno statico $ \vec{E}_0 = E_0 \hat{\vec{z}} $ e all'equilibrio termodinamico a temperatura $ T $. L'hamiltoniana della molecola avrà la forma $ H = K - p E_0 \cos{\theta} $ dove $ K $ è l'energia cinetica e $ \theta $ è l'angolo formato da $ \vec{p} $ con l'asse $ z $. Assumendo la distribuzione di Boltzmann vogliamo calcolare il momento di dipolo medio della molecola: per simmetria il valore medio di $ \vec{p} $ nel piano $ xy $ sarà nullo, pertanto ci basta calcolare $ \langle p \cos{\theta} \rangle $. Da una nota relazione della meccanica statistica si ha che
	\[
		\langle p \cos{\theta} \rangle = - \frac{1}{\beta} \pd{}{(- E_0)} \log{Z} = \frac{1}{\beta} \pd{}{E_0} \log{Z}
	\]
	dove $ \beta = 1/k_B T $. Ora la funzione di partizione della singola particella è
	\[
		Z = \int e^{-\beta H} \dif{\Gamma}  = \int e^{-\beta K} e^{\beta p E_0 \cos{\theta}} \dif{\Gamma} = \alpha \int_{-1}^{1} e^{\beta p E_0 \cos{\theta}} \dif{\cos\theta}
	\]
	dove abbiamo indicato con $ \dif{\Gamma} $ l'elemento dello spazio delle fasi e nel secondo passaggio abbiamo su tutto tranne che in $ \theta $ tale integrazione porta ad una costante di integrazione moltiplicativa indipendente da $ E_0 $ indicata con $ \alpha $. Quindi
	\[
		Z = \alpha \int_{-1}^{1} e^{\beta p E_0 \cos{\theta}} \dif{\cos\theta} = \alpha \left[\frac{e^{\beta p E_0}}{\beta p E_0} - \frac{e^{-\beta p E_0}}{\beta p E_0}\right] = \alpha \frac{\sinh{(\beta p E_0)}}{\beta p E_0}.
	\]
	Pertanto
	\begin{align*}
		\langle p \cos{\theta} \rangle & = \frac{1}{\beta} \dpd{}{E_0} \log{Z} = \frac{1}{\beta} \dpd{}{E_0} \left(\log{\alpha} + \log{\sinh{(\beta p E_0)}} - \log{\beta p} - \log{E_0}\right) \\
		& = \frac{1}{\beta} \dpd{}{E_0} \left(\log{\sinh{(\beta p E_0)}} - \log{E_0}\right) = \frac{1}{\beta} \left(\coth{(\beta p E_0)} \beta p - \log{E_0}\right) \\
		& = p \left(\coth{(\beta p E_0)} - \frac{1}{\beta p E_0}\right)
	\end{align*}
	In generale quindi la polarizzazione del gas sarà non lineare: potevamo aspettarci tale risultato osservando che nel limite di bassa temperatura tutti i dipoli del gas tenderanno ad essere allineati con il campo esterno fino a raggiungere un regime di saturazione Se ci mettiamo però nel limite di alta temperatura, ovvero per $ x \coloneqq \beta p E_0 \ll 1 $ possiamo sviluppare l'espressione precedente
	\begin{align*}
		\langle \cos{\theta} \rangle & = \frac{\cosh x}{\sinh x} - \frac{1}{x} \simeq \frac{1 + \frac{1}{2} x^2}{x + \frac{1}{6} x^3} - \frac{1}{x} = \frac{1}{x} \left(\frac{1 + \frac{1}{2} x^2}{1 + \frac{1}{6} x^2} - 1\right) \\
		& \simeq \frac{1}{x} \left[\left(1 + \frac{1}{2} x^2\right)\left(1 - \frac{1}{6} x^2\right) - 1\right] \simeq \frac{x}{3}
	\end{align*}
	Così se il gas è composto da $ n $ molecole per densità di volume la polarizzazione è
	\[
		\vec{P} = n \langle \vec{p} \rangle = n p \langle \cos{\theta}\rangle \hat{\vec{z}} \simeq n p \frac{\beta p E_0}{3} \hat{\vec{z}} = \frac{n p^2}{3 k_B T} \vec{E}_0.
	\]
	Abbiamo quindi ottenuto che per alta temperatura la suscettività è $ \chi \simeq \frac{n p^2}{3 k_B T} $.
\end{proof}

\begin{dissertazione} \label{diss:reflex}
    La riflessione totale interna.
\end{dissertazione}

\begin{dissertazione} \label{diff:potenziali}
    I potenziali elettrodinamici.
\end{dissertazione}

\begin{dissertazione} \label{diss:onde}
    Propagazione delle onde elettromagnetiche in un dielettrico trasparente e dispersivo.
\end{dissertazione}

\begin{dissertazione} \label{diss:lorentz}
    I postulati della relatività speciale.
\end{dissertazione}

\begin{dissertazione} \label{diss:corrente}
    Corrente di magnetizzazione e di polarizzazione.
\end{dissertazione}

\begin{dissertazione} \label{diss:densita}
    La densità di energia elettromagnetica.
\end{dissertazione}


\begin{table}[h]
    \centering
    \begin{tabular}{@{}cccccccccccccccc@{}}
        \toprule
        D. & 21 & 19 & 18 & 17 & 16 & 15 & 14 & 13 & 12 & 11 & 10 & 09 & 08 & 07 & Tot \\ \midrule
        \ref{diss:energia} & $\bullet$ & $\bullet$ & $\bullet$ & $\bullet$ &  & $\bullet$ &  &  & $\bullet$ & $\bullet$ & $\bullet$ &  &  & $\bullet$ & 9 \\
        \ref{diss:polaritoni} &  &  & $\bullet$ & $\bullet$ & $\bullet$ & $\bullet$ & $\bullet$ &  & $\bullet$ & $\bullet$ &  &  &  & $\bullet$ & 8 \\
        \ref{diss:kk} &  &  & $\bullet$ &  &  &  &  &  & $\bullet$ &  & $\bullet$ & $\bullet$ & $\bullet$ & $\bullet$ & 6 \\
        \ref{diss:nonlin} & $\bullet$ & $\bullet$ & $\bullet$ & $\bullet$ & $\bullet$ &  &  & $\bullet$ &  &  &  &  &  &  & 6 \\
        \ref{diss:locale} &  &  &  &  & $\bullet$ &  &  & $\bullet$ &  &  & $\bullet$ &  & $\bullet$ &  & 4 \\
        \ref{diss:gaspolare} & $\bullet$ & $\bullet$ &  &  &  &  &  &  &  &  &  & $\bullet$ &  &  & 3 \\
        \ref{diss:reflex} &  &  &  &  &  & $\bullet$ &  &  &  &  &  & $\bullet$ &  &  & 2 \\
        \ref{diff:potenziali} &  &  &  &  &  &  & $\bullet$ &  &  &  &  &  &  &  & 1 \\
        \ref{diss:onde} &  &  &  &  &  &  & $\bullet$ &  &  &  &  &  &  &  & 1 \\
        \ref{diss:lorentz} &  &  &  &  &  &  &  & $\bullet$ &  &  &  &  &  &  & 1 \\
        \ref{diss:corrente} &  &  &  &  &  &  &  &  &  & $\bullet$ &  &  &  &  & 1 \\
        \ref{diss:densita} &  &  &  &  &  &  &  &  &  &  &  &  & $\bullet$ &  & 1 \\ \bottomrule
    \end{tabular}
    \label{tab:conteggioDissertazioni}
    \caption{conteggio delle dissertazioni.}
\end{table}


\end{document}
